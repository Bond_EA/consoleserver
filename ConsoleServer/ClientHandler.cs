﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ConsoleServer
{
    public class ClientHandler :IDisposable
    {
        Server server;

        public Socket tcpHandler;
        //public Socket udpHandler;

        //IPEndPoint remoteEpUdp;

        public int id;
        public string ip;

        Thread threadListenTcp;

        
        public ClientHandler(Server server, Socket tcpHandler, int id)
        {
            // init variables
            this.server = server;
            this.tcpHandler = tcpHandler;
            this.id = id;
            ip = this.GetRemoteIp();

            // init Upd sending
            //udpHandler = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            //remoteEpUdp = new IPEndPoint(IPAddress.Any, Server.portUdp);

            // start Tcp reading
            threadListenTcp = new Thread(new ThreadStart(ListenTcp));
            threadListenTcp.Start();
        }

        int errorMessages = 0;
        void ListenTcp()
        {
            byte[] bytes = new byte[1024];
            string str;

            while (true)
            {
                try
                {
                    str = ReadLine2(tcpHandler, bytes);
                    if (!str.Equals(""))
                    {
                        Console.WriteLine($"[TCP][{ip}]: {str}");
                    }
                    else
                    {
                        errorMessages++;
                        if(errorMessages > 100)
                        {
                            ShutDownClient(1);
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    ShutDownClient(2);
                    break;
                }
            }
        }

        string ReadLine(Socket reciever, byte[] buffer)
        {
            int bytesRec = reciever.Receive(buffer);
            string data = Encoding.ASCII.GetString(buffer, 0, bytesRec);
            return data;
        }

        string ReadLine2(Socket reciever, byte[] buffer)
        {
            StringBuilder builder = new StringBuilder();
            int bytes = 0; // количество полученных байтов

            do
            {
                bytes = reciever.Receive(buffer);
                builder.Append(Encoding.Unicode.GetString(buffer, 0, bytes));
            }
            while (tcpHandler.Available > 0);

            //Console.WriteLine(DateTime.Now.ToShortTimeString() + ": " + builder.ToString());
            return builder.ToString();
        }
        public void SendMessageTcp(string message)
        {
            byte[] dataToSend = Encoding.Unicode.GetBytes(message);
            tcpHandler.Send(dataToSend);
        }

        public void SendMessageUdp(string message)
        {
            server.SendMessageUdp(message, ip);
            //byte[] data = Encoding.Unicode.GetBytes(message);
            //udpHandler.SendTo(data, remoteEpUdp);
        }

        void ShutDownClient(int error = 0)
        {
            server.RemoveClient(this, error);
            tcpHandler.Shutdown(SocketShutdown.Both);
            tcpHandler.Dispose();
            //udpHandler.Shutdown(SocketShutdown.Both);
            //udpHandler.Dispose();
            Dispose();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    tcpHandler.Shutdown(SocketShutdown.Both);
                    tcpHandler.Close();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~ClientHandler()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }
}
