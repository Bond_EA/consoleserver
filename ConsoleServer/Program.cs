﻿using System;
using System.Runtime.InteropServices;

namespace ConsoleServer
{
    class Program
    {
        static void Main(string[] args)
        {
            new Server();
        }

        #region Console closing related - Don't touch
        // Don't know how it works but will figure it out.

        // https://msdn.microsoft.com/fr-fr/library/windows/desktop/ms686016.aspx
        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(SetConsoleCtrlEventHandler handler, bool add);

        // https://msdn.microsoft.com/fr-fr/library/windows/desktop/ms683242.aspx
        private delegate bool SetConsoleCtrlEventHandler(CtrlType sig);

        private enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }

        private static bool Handler(CtrlType signal)
        {
            switch (signal)
            {
                case CtrlType.CTRL_BREAK_EVENT:
                case CtrlType.CTRL_C_EVENT:
                case CtrlType.CTRL_LOGOFF_EVENT:
                case CtrlType.CTRL_SHUTDOWN_EVENT:
                case CtrlType.CTRL_CLOSE_EVENT:
                    Console.WriteLine("Closing");

                    Environment.Exit(0);
                    return false;

                default:
                    return false;
            }
        }
        #endregion
    }
}

