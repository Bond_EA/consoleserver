﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ConsoleServer
{
    public static class ClientHandlerExtensions
    {
        public static bool SocketSimpleConnected(this ClientHandler ch)
        {
            return !((ch.tcpHandler.Poll(1000, SelectMode.SelectRead) && (ch.tcpHandler.Available == 0)) || !ch.tcpHandler.Connected);
        }

        public static string GetRemoteIp(this ClientHandler ch)
        {
            string rawRemoteIP = ch.tcpHandler.RemoteEndPoint.ToString();
            int dotsIndex = rawRemoteIP.LastIndexOf(":");
            string remoteIP = rawRemoteIP.Substring(0, dotsIndex);
            return remoteIP;
        }
        public static string GetRemoteIpAndPort(this ClientHandler ch)
        {
            return ch.tcpHandler.RemoteEndPoint.ToString();
        }
    }
}
