﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ConsoleServer
{
    class ConsoleListener
    {
        Thread mainThread;
        Server server;

        public ConsoleListener(Server server)
        {
            this.server = server;

            mainThread = new Thread(new ThreadStart(Run));
            mainThread.Start();
        }

        void Run()
        {
            while (true)
            {
                string textToSend = Console.ReadLine();
                ManageString(textToSend);
            }
        }

        void ManageString(string str)
        {
            if (str.StartsWith("|"))
            {
                string[] stringArray = str.Split('|');

                /*
                  id|2|tcp|hello
                  tcp|bro
                  hello there
                  udp|gggg
                  id|6|My name is Tom              
                 */

                bool sendToSpecId = false;
                bool sendTcp = false;
                bool sendUdp = false;
                int specId = 0;

                for (int i = 0; i < stringArray.Length; i++)
                {
                    if (stringArray[i].Equals("id"))
                    {
                        sendToSpecId = true;
                        specId = Int32.Parse(stringArray[i + 1]);
                    }
                        
                    if (stringArray[i].Equals("tcp"))
                    {
                        sendTcp = true;
                        sendUdp = false;
                    }
                    if (stringArray[i].Equals("udp"))
                    {
                        sendTcp = false;
                        sendUdp = true;
                    }
                }

                string pureMessage = str.Replace("id", "")
                    .Replace("tcp", "")
                    .Replace("udp", "")
                    .Replace("|", "");

                server.SendMessage(pureMessage, !sendToSpecId, specId, sendTcp, sendUdp);
            }
            else
            {
                server.SendMessageToAllClientsTcp(str);
            }
        }
    }
}
