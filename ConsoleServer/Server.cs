﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleServer
{
    public class Server
    {
        public static int portTcp = 8384; // correct port for Tcp
        public static int portUdp = 8385; // correct port for Udp

        private Dictionary<int, ClientHandler> clients;

        static IPEndPoint ipEndPointTcp;
        static IPEndPoint ipEndPointUdp;

        Socket listenSocketTcp;
        static Socket listenSocketUdp;

        public Server()
        {
            Run();
        }

        void Run()
        {
            clients = new Dictionary<int, ClientHandler>();
            new ConsoleListener(this);
            WriteAddressToConsole();

            ipEndPointTcp = new IPEndPoint(IPAddress.Any, portTcp);
            ipEndPointUdp = new IPEndPoint(IPAddress.Any, portUdp);
            listenSocketTcp = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            listenSocketUdp = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            // launch UDP listener
            Task listeningTask = new Task(ListenUDP);
            listeningTask.Start();

            // launch TCP process↓
            try
            {
                listenSocketTcp.Bind(ipEndPointTcp);
                listenSocketTcp.Listen(5);

                while (true)
                {
                    Socket tcpSocketHandler = listenSocketTcp.Accept();

                    int clientId = GetFirstFreeId();
                    ClientHandler a = new ClientHandler(this, tcpSocketHandler, clientId);
                    AddClient(a, clientId);
                }
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                if (listenSocketTcp != null)
                {
                    listenSocketTcp.Shutdown(SocketShutdown.Both);
                    listenSocketTcp.Close();
                    listenSocketTcp = null;
                }
            }
        }
        #region Listen UPD - doesn't require established connection
        private static void ListenUDP()
        {
            try
            {
                listenSocketUdp.Bind(ipEndPointUdp);
                byte[] data = new byte[1024];
                EndPoint remoteIp = new IPEndPoint(IPAddress.Any, portUdp);
                
                int bytes;
                while (true)
                {
                    StringBuilder builder = new StringBuilder();
                    do
                    {
                        bytes = listenSocketUdp.ReceiveFrom(data, ref remoteIp);
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (listenSocketUdp.Available > 0);

                    IPEndPoint remoteFullIp = remoteIp as IPEndPoint;
                    Console.WriteLine($"[UDP][{remoteFullIp.Address}]: {builder}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                CloseUdp();
            }
        }
        private static void CloseUdp()
        {
            if (listenSocketUdp != null)
            {
                listenSocketUdp.Shutdown(SocketShutdown.Both);
                listenSocketUdp.Close();
                listenSocketUdp = null;
            }
        }
        #endregion


        #region Add | Remove client
        public void AddClient(ClientHandler a, int id)
        {
            Console.WriteLine($"[TCP CONNECT][{a.GetRemoteIp()}]");
            clients[id] = a;
        }
        int GetFirstFreeId()
        {
            ClientHandler util;
            for (int i = 1; i < 10000; i++)
            {
                if(!clients.TryGetValue(i, out util))
                {
                    return i;
                }
            }
            Console.WriteLine("Error getting first free id!");
            return -1;
        }
        ClientHandler TryToGetClientWithId(int id)
        {
            ClientHandler util;
            if (clients.TryGetValue(id, out util))
            {
                return util;
            }
            else
            {
                Console.WriteLine($"Error getting client with id {id}");
                return null;
            }
        }
        // errorNumber 1 = Glitched and started spamming "" text
        public void RemoveClient(ClientHandler a, int errorNumber = 0)
        {
            if(errorNumber == 0) Console.WriteLine($"[TCP DISCONNECT][{a.GetRemoteIp()}]");
            else Console.WriteLine($"[TCP DISCONNECT][{a.GetRemoteIp()}] - error number: {errorNumber}");
            clients.Remove(a.id);
        }
        #endregion

        #region Util
        public void SendMessageToAllClientsTcp(string message)
        {
            for (int i = 1; i <= clients.Count; i++)
            {
                clients[i].SendMessageTcp(message);
            }
        }
        public void SendMessageToAllClientsUdp(string message)
        {
            for (int i = 1; i <= clients.Count; i++)
            {
                clients[i].SendMessageUdp(message);
            }
        }

        public void SendMessageUdp(string message, string ip)
        {
            Console.WriteLine($"Trying to send UDP message to ip {ip}");
            EndPoint remotePoint = new IPEndPoint(IPAddress.Parse(ip), portUdp);
            byte[] data = Encoding.Unicode.GetBytes(message);
            listenSocketUdp.SendTo(data, remotePoint); //remoteEpUdp
        }
        public void SendMessage(string message, bool toAll = true, int idOfSpecClient = 0, bool tcp = true, bool udp = false)
        {
            if (toAll)
            {
                if (tcp)
                {
                    SendMessageToAllClientsTcp(message);
                }else if (udp)
                {
                    SendMessageToAllClientsUdp(message);
                }
                else Console.WriteLine("Error of choice 1");
            }
            else
            {
                ClientHandler clientToSend = TryToGetClientWithId(idOfSpecClient);
                if(clientToSend != null)
                {
                    if (tcp)
                    {
                        clientToSend.SendMessageTcp(message);
                    }else if (udp)
                    {
                        clientToSend.SendMessageUdp(message);
                    }
                    else Console.WriteLine("Error of choice 2");
                }
            }
        }

        void WriteAddressToConsole()
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[1];
            Console.WriteLine("Address = " + ipAddress);
            Console.WriteLine("_____________________\n");
        }
        #endregion
    }
}
 

